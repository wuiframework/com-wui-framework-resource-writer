# com-wui-framework-resource-writer v2019.1.0

> Multi-platform library that can inject (and extract) arbitrary content into binary files (including executable ones such as PE, ELF, Mach-O).

## Requirements

This application depends on the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Added full implementation for Linux, ARM and OS X. Added caching mechanism and resource update schema.
### v2019.0.0
Initial version for NodejsRE integration.

## License

This software is owned or controlled by NXP Semiconductors.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Karel Burda, 
Copyright (c) 2019 [NXP](http://nxp.com/)
