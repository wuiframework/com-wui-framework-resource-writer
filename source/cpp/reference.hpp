/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_RESOURCEWRITER_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_RESOURCEWRITER_HPP_

// generated-code-start
#include "Com/Wui/Framework/ResourceWriter/Core/Detail/BinaryReader.hpp"
#include "Com/Wui/Framework/ResourceWriter/Core/Protocol.hpp"
#include "Com/Wui/Framework/ResourceWriter/Core/Resource.hpp"
#include "Com/Wui/Framework/ResourceWriter/Logger/Logger.hpp"
#include "Com/Wui/Framework/ResourceWriter/Logger/LoggerConsole.hpp"
#include "Com/Wui/Framework/ResourceWriter/Logger/LoggerDevNull.hpp"
#include "Com/Wui/Framework/ResourceWriter/Logger/LogManager.hpp"
#include "Com/Wui/Framework/ResourceWriter/Reader/FileReader.hpp"
#include "Com/Wui/Framework/ResourceWriter/Reader/Resource.hpp"
#include "Com/Wui/Framework/ResourceWriter/Reader/ResourceReader.hpp"
#include "Com/Wui/Framework/ResourceWriter/Reader/Settings.hpp"
#include "Com/Wui/Framework/ResourceWriter/ResourceWriter.hpp"
#include "Com/Wui/Framework/ResourceWriter/Utils/ByteUtils.hpp"
#include "Com/Wui/Framework/ResourceWriter/Utils/CollectionUtils.hpp"
#include "Com/Wui/Framework/ResourceWriter/Utils/FileUtils.hpp"
#include "Com/Wui/Framework/ResourceWriter/Writer/Detail/FileWriter.hpp"
#include "Com/Wui/Framework/ResourceWriter/Writer/Resource.hpp"
#include "Com/Wui/Framework/ResourceWriter/Writer/ResourceWriter.hpp"
#include "Com/Wui/Framework/ResourceWriter/Writer/Settings.hpp"
#include "interfacesMap.hpp"
// generated-code-end

#endif  // COM_WUI_FRAMEWORK_RESOURCEWRITER_HPP_ // NOLINT
