/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef COM_WUI_FRAMEWORK_RESOURCEWRITER_CORE_INTERFACESMAP_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_RESOURCEWRITER_CORE_INTERFACESMAP_HPP_

namespace Com {
    namespace Wui {
        namespace Framework {
            namespace ResourceWriter {
                namespace Core {
                    class Resource;
                    namespace Detail {
                        class BinaryReader;
                    }
                }
                namespace Logger {
                    class Logger;
                    class LoggerConsole;
                    class LoggerDevNull;
                    class LogManager;
                }
                namespace Reader {
                    struct ProtocolError;
                    class ResourceReader;
                    class Settings;
                }
                namespace Utils {
                }
                namespace Writer {
                    class ResourceWriter;
                    class Settings;
                    namespace Detail {
                        class FileWriter;
                    }
                }
            }
        }
    }
}

#endif  // COM_WUI_FRAMEWORK_RESOURCEWRITER_CORE_INTERFACESMAP_HPP_  // NOLINT
