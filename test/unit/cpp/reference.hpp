/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_RESOURCEWRITER_TEST_REFERENCE_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_RESOURCEWRITER_TEST_REFERENCE_HPP_

#include "../../../source/cpp/reference.hpp"
#include <gtest/gtest.h>

#endif  // COM_WUI_FRAMEWORK_RESOURCEWRITER_TEST_REFERENCE_HPP_  NOLINT
